﻿using UnityEngine;

public class ConstructorCell : MonoBehaviour
{
    [SerializeField] private GameObject _indicator;

    private GameObject _tile;

    public GameObject Tile
    {
        get { return _tile;}
        set
        {
            _tile = value;
            if (_tile == null)
            {
                _indicator.SetActive(true);
            }
            else
            {
                _indicator.SetActive(false);
            }
        }
    }

    public Vector2Int Pos { get; set; }
}
