﻿using UnityEngine;
using UnityEngine.UI;

public class TileCheckBox : MonoBehaviour
{
    [SerializeField] private Text _text;

    public GameObject Tile { get; set; }

    public void Initialize(GameObject go)
    {
        Tile = go;
        _text.text = go.GetComponent<TileInfo>().TileName;
    }
}
