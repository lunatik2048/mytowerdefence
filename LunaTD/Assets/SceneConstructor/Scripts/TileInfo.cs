﻿using UnityEngine;

public class TileInfo : MonoBehaviour
{
    [SerializeField] private int _tileId;
    [SerializeField] private string _tileName;
    [SerializeField] private bool _isStart;
    [SerializeField] private bool _isFinish;

    public int TileId
    {
        get { return _tileId;}
    }

    public string TileName
    {
        get { return _tileName; }
    }

    public bool IsStart
    {
        get { return _isStart; }
    }

    public bool IsFinish
    {
        get { return _isFinish; }
    }
}
