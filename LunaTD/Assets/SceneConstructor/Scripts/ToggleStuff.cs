﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ToggleStuff : MonoBehaviour
{
    [SerializeField] private GameObject _togglePrefab;
    [SerializeField] private GameObject[] _tiles;

    private ToggleGroup _tGroup;

    public static ToggleStuff Instance;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _tGroup = GetComponent<ToggleGroup>();

        foreach (var tile in _tiles)
        {
            var g = Instantiate(_togglePrefab, transform);
            g.GetComponent<Toggle>().group = _tGroup;
            g.GetComponent<TileCheckBox>().Initialize(tile);
        }
    }

    public TileCheckBox GetSelected()
    {
        if (_tGroup.AnyTogglesOn())
        {
            return _tGroup.ActiveToggles().First().GetComponent<TileCheckBox>();
        }
        else
        {
            return null;
        }
    }
}
