﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CellSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _cell;
    [SerializeField] private InputField _saveName;
    [SerializeField] private InputField _loadName;

    [SerializeField] private InputField _rows;
    [SerializeField] private InputField _columns;

    [SerializeField] private TileInfo[] _tiles;

    private List<ConstructorCell> _dataToSave;

    public void Create()
    {
        _dataToSave = new List<ConstructorCell>();
        Spawn(Convert.ToInt32(_columns.text), Convert.ToInt32(_rows.text));
    }

    void Spawn(int columns, int rows)
    {
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                var c = Instantiate(_cell, new Vector3(i, 0,j), Quaternion.identity);
                c.GetComponent<ConstructorCell>().Pos = new Vector2Int(i,j);
                _dataToSave.Add(c.GetComponent<ConstructorCell>());
            }
        }
    }

    public void Save()
    {
        var l = new CellTileData[_dataToSave.Count];
        int i = 0;
        foreach (var constructorCell in _dataToSave)
        {
            var data = new CellTileData();
            data.coordinates = constructorCell.Pos;
            if (constructorCell.Tile != null)
            {
                var tileInfo = constructorCell.Tile.GetComponent<TileInfo>();
                data.tileId = tileInfo.TileId;
                data.rotation = constructorCell.Tile.transform.rotation;
                data.isFinish = tileInfo.IsFinish;
                data.isStart = tileInfo.IsStart;
            }
            l[i] = data;
            i++;
        }
        var str = JsonHelper.ToJson(l,true);
        SaveItemInfo(str);
    }

    public void SaveItemInfo(string str)
    {
        var path = "Assets/CustomLevels/" + _saveName.text + ".json";
        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }
    }

    public void Load()
    {
        _dataToSave = new List<ConstructorCell>();
        string str = string.Empty;
        var path = "Assets/CustomLevels/" + _loadName.text + ".json";
        using (FileStream fs = new FileStream(path, FileMode.Open))
        {
            using (StreamReader reader = new StreamReader(fs))
            {
                str = reader.ReadToEnd();
            }
        }
        var res = JsonHelper.FromJson<CellTileData>(str);
        Spawn(res);
    }

    private void Spawn(CellTileData[] cells)
    {
        foreach (var cellTileData in cells)
        {
            int i = cellTileData.coordinates.x;
            int j = cellTileData.coordinates.y;
            var c = Instantiate(_cell, new Vector3(i, 0, j), Quaternion.identity);
            var constrCell = c.GetComponent<ConstructorCell>();
            constrCell.Pos = new Vector2Int(i, j);
            _dataToSave.Add(constrCell);
            var t = GetTileWithId(cellTileData.tileId);
            if (t != null)
            {
                constrCell.Tile = Instantiate(t.gameObject, c.transform);
                constrCell.Tile.transform.rotation = cellTileData.rotation; 
            }
        }
    }

    private TileInfo GetTileWithId(int id)
    {
        foreach (var tileInfo in _tiles)
        {
            if (tileInfo.TileId == id)
                return tileInfo;
        }
        return null;
    }
}
