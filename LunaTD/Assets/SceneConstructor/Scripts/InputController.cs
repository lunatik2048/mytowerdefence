﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    private ToggleStuff _tileSelector;
    private MouseController _mouseController;


    // Start is called before the first frame update
    void Start()
    {
        _tileSelector = ToggleStuff.Instance;
        _mouseController = MouseController.Instance;
    }

    void Update()
    {
        if (_mouseController.CurrentCell != null)
        {
            BuildTile();
            DestroyTile();
            RotateTile();
        }
    }

    void BuildTile()
    {
        if (Input.GetMouseButton(0))
        {
            var t = _mouseController.CurrentCell.Tile;
            if (t != null)
                Destroy(t);
            var newTile = Instantiate(ToggleStuff.Instance.GetSelected().Tile, _mouseController.CurrentCell.transform);
            _mouseController.CurrentCell.Tile = newTile;
        }
    }

    void DestroyTile()
    {
        if (Input.GetMouseButton(1))
        {
            var t = _mouseController.CurrentCell.Tile;
            if (t != null)
            {
                Destroy(t);
                _mouseController.CurrentCell.Tile = null;
            }
        }
    }

    void RotateTile()
    {
        var scroll = Input.mouseScrollDelta.y;

        if (scroll != 0)
        {
            var t = _mouseController.CurrentCell.Tile;
            if (t != null)
            {
                t.transform.Rotate(new Vector3(0, 1, 0), 90 * scroll);
            }
        }
    }
}
