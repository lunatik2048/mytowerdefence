﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MouseController : MonoBehaviour
{
    private int _layerMask;

    public ConstructorCell CurrentCell { get; set; }

    public static MouseController Instance;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _layerMask = LayerMask.GetMask("Constructor");
    }

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            CurrentCell = null;
            return;
        }
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000, _layerMask))
        {
            var selected = hit.collider.gameObject;
            CurrentCell = selected.GetComponent<ConstructorCell>();
        }
        else
        {
            CurrentCell = null;
        }
    }
}
