﻿using System;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    private int _money;

    public static MoneyManager Instance;

    public Action<int> MoneyChanged;

    public int Money
    {
        get { return _money;}
        private set
        {
            _money = value;
            MoneyChanged?.Invoke(_money);
        }
    }

    void Awake()
    {
        Instance = this;
    }

    public void Initialize(int startingMoney)
    {
        Money = startingMoney;
    }

    public void AddMoney(int amount)
    {
        Money += amount;
    }

    public bool SpendMoney(int amount)
    {
        if (Money - amount < 0)
        {
            return false;
        }
        else
        {
            Money -= amount;
            return true;
        }
    }

}
