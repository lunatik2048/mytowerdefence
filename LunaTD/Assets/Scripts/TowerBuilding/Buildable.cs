﻿using System;
using UnityEngine;

public class Buildable : MonoBehaviour
{
    [SerializeField] private float  _buildHeight;

    private bool _hasTower;
    private GameObject _tower;

    public Action<Buildable> SellStateChanged;

    public Vector3 BuildHeight
    {
        get;
        private set;
    }

    private void Start()
    {
        var pos = transform.position;
        pos.y = _buildHeight + 0.01f;
        BuildHeight = pos;
    }

    public GameObject Tower
    {
        get { return _tower; }
        set
        {
            _tower = value;
            if (value == null)
            {
                HasTower = false;
            }
            else
            {
                HasTower = true;
            }
        }
    }

    public bool HasTower
    {
        get { return _hasTower; }
        private set
        {
            _hasTower = value;
            SellStateChanged?.Invoke(this);
        }
    }

    public void BuildTower(GameObject prefab)
    {
        var tower = Instantiate(prefab, BuildHeight, Quaternion.identity, gameObject.transform.parent);
        Tower = tower;
    }
}