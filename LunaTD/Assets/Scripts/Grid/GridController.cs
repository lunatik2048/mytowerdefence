﻿using System.IO;
using UnityEngine;
using UnityEngine.AI;

public class GridController : MonoBehaviour
{
    [SerializeField] private TileInfo[] _tiles;

    private Cell[,] _cells;
    private string _gridContainerName = "Grid";

    public Cell[,] Cells { get { return _cells; } }

    public Vector2Int StartPoint { get; private set; }

    public Vector2Int FinishPoint { get; private set; }

    public static Vector3 GetWorldCoordinates(Vector2Int coord)
    {
        return new Vector3(coord.x * Constants.CellSize, Constants.FlourHeight, coord.y * Constants.CellSize);
    }

    public void CreateGrid(string lvlName)
    {
        var go = new GameObject(_gridContainerName);
        var nav = go.AddComponent<NavMeshSurface>();
        var parent = go.transform;
        Load(lvlName, parent);

        nav.buildHeightMesh = true;
        nav.BuildNavMesh();
    }

    public void Load(string fileName, Transform parent)
    {
        string str = string.Empty;
        var path = Constants.LevelsLocation + fileName + Constants.Json;
        using (FileStream fs = new FileStream(path, FileMode.Open))
        {
            using (StreamReader reader = new StreamReader(fs))
            {
                str = reader.ReadToEnd();
            }
        }
        var res = JsonHelper.FromJson<CellTileData>(str);
        Spawn(res, parent);
    }

    private void Spawn(CellTileData[] cells, Transform parent)
    {
        var l = GetArrayLength(cells);
        _cells = new Cell[l.x + 1, l.y + 1];


        foreach (var cellTileData in cells)
        {
            if (cellTileData.tileId != 0)
            {
                int i = cellTileData.coordinates.x;
                int j = cellTileData.coordinates.y;

                var t = GetTileWithId(cellTileData.tileId);

                InstantiateCell(t.gameObject, parent, i, j, cellTileData.rotation);

                if (t.IsFinish)
                    FinishPoint = new Vector2Int(i,j);
                else if (t.IsStart)
                    StartPoint = new Vector2Int(i, j);
            }
        }
    }

    private TileInfo GetTileWithId(int id)
    {
        foreach (var tileInfo in _tiles)
        {
            if (tileInfo.TileId == id)
                return tileInfo;
        }
        return null;
    }

    private void InstantiateCell(GameObject prefab, Transform parent, int x, int y, Quaternion rot)
    {
        var cellObj = Instantiate(prefab);
        var cellTransform = cellObj.transform;
        cellTransform.rotation = rot;
        cellTransform.SetParent(parent.transform);
        cellTransform.position = GetWorldCoordinates(new Vector2Int(x, y));
        var cell = cellObj.GetComponent<Cell>();
        _cells[x, y] = cell;
    }

    private Vector2Int GetArrayLength(CellTileData[] cells)
    {
        Vector2Int v = new Vector2Int(0, 0);
        foreach (var c in cells)
        {
            if (c.coordinates.x > v.x)
            {
                v.x = c.coordinates.x;
            }

            if (c.coordinates.y > v.y)
            {
                v.y = c.coordinates.y;
            }
        }

        return v;
    }
}
