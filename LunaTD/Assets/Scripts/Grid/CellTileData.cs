﻿using System;
using UnityEngine;

[Serializable]
public class CellTileData 
{
    public Vector2Int coordinates;
    public int tileId;
    public Quaternion rotation;
    public bool isStart;
    public bool isFinish;
}
