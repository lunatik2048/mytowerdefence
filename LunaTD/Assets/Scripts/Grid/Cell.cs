﻿using UnityEngine;

public class Cell : MonoBehaviour
{
	private Vector2Int _cellPositions;

	public Vector2Int CellPosition 
	{
		get { return _cellPositions; }
		set { _cellPositions = value; }
	}
}
