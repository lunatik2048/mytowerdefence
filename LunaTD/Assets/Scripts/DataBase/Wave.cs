﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Wave")]
public class Wave : ScriptableObject
{
    [SerializeField]
    public MonstersPack[] packs;
}
