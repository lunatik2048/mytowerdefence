﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Mission")]
public class LevelInfo : ScriptableObject
{
    public int lives;
    public int startingMoney;
    public Wave[] waves;
}
