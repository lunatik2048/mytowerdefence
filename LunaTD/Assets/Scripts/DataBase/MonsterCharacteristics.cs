﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Monsters/Characteristics")]
public class MonsterCharacteristics : ScriptableObject
{
    public MonstersId id;
    public string name;
    public float maxHitpoints;
    public float movementSpeed;
    public int liveReduction;
    public int money;
    public int numOfMinions;
    public GameObject prefab;
}
