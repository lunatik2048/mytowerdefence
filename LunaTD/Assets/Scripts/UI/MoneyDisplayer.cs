﻿using UnityEngine;
using UnityEngine.UI;

public class MoneyDisplayer : MonoBehaviour
{
    [SerializeField] private Text _moneyText;
    [SerializeField] private MoneyManager _manager;

    void Start()
    {
        _moneyText.text = _manager.Money.ToString();
        _manager.MoneyChanged+= UpdateUI;
    }

    private void UpdateUI(int value)
    {
        _moneyText.text = value.ToString();
    }
}
