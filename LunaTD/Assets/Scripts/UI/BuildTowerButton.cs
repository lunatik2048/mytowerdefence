﻿using UnityEngine;
using UnityEngine.UI;

public class BuildTowerButton : MonoBehaviour
{
    [SerializeField] private GameObject _towerPrefab;
    [SerializeField] private Button _button;
    [SerializeField] private Text _name;
    [SerializeField] private Text _cost;
    [SerializeField] private TowerBuildSelector _towerSel;

    private MoneyManager _moneyManager;

    void Start()
    {
        _moneyManager = MoneyManager.Instance;
        if (_towerPrefab == null)
        {
            return;
        }
        _cost.text = _towerPrefab.GetComponent<Cost>().MoneyCost.ToString();
        _name.text = _towerPrefab.GetComponent<Cost>().Name;
        _moneyManager.MoneyChanged += MoneyChanged;
        MoneyChanged(_moneyManager.Money);
    }

    private void MoneyChanged(int obj)
    {
        if (obj > _towerPrefab.GetComponent<Cost>().MoneyCost)
        {
            _button.interactable = true;
        }
        else
        {
            _button.interactable = false;
        }
    }

    public void OnButtonPressed()
    {
        _towerSel.TryBuilding(_towerPrefab, this);
    }

    public void SelectTower(bool isSelected)
    {
        if (isSelected)
        {
            _button.GetComponent<Image>().color = Color.grey;
        }
        else
        {
            _button.GetComponent<Image>().color = Color.white;
        }
    }
}
