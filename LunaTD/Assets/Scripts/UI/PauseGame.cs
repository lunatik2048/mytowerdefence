﻿using UnityEngine;

public class PauseGame : MonoBehaviour
{
    [SerializeField] private WindowBase _window;

    public void Pause()
    {
        _window.Open();
        TimeManager.PauseGame();
    }

    public void Resume()
    {
        _window.Close();
        TimeManager.ResumeGame();
    }

    public void GoToMainMenu()
    {
        SceneLoader.LoadMainMenuScene();
    }

    public void Exit()
    {
        SceneLoader.QuitGame();
    }
}
