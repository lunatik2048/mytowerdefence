﻿using UnityEngine;
using UnityEngine.UI;

public class HitpointsDisplayer : MonoBehaviour
{
    [SerializeField] private Hitpoints _hp;

    private Image _sprRenderer;

    void Start()
    {
        _sprRenderer = GetComponent<Image>();
    }

    void Update()
    {
        _sprRenderer.fillAmount = _hp.currentHP / _hp.maxHP;
    }
}
