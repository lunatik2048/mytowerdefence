﻿using UnityEngine;
using UnityEngine.UI;

public class TowerInfoDisplayer : MonoBehaviour
{
    [SerializeField] private Text _damage;
    [SerializeField] private Text _attackSpeed;
    [SerializeField] private Text _radius;
    [SerializeField] private Text _damageDone;

    private StatisticsCounter _dmgDone;

    public void Show(float dmg, float aSpeed, float radius, StatisticsCounter dmgDone)
    {
        gameObject.SetActive(true);
        _damage.text = dmg.ToString();
        _attackSpeed.text = aSpeed.ToString();
        _radius.text = radius.ToString();
        _damageDone.text = dmgDone.Damage.ToString();
        _dmgDone = dmgDone;
        _dmgDone.DamageChanged += DmgDoneOnDamageChanged;
    }

    private void DmgDoneOnDamageChanged(float obj)
    {
        _damageDone.text = obj.ToString();
    }

    public void Hide()
    {
        if (_dmgDone != null)
        {
            _dmgDone.DamageChanged -= DmgDoneOnDamageChanged;
        }
        gameObject.SetActive(false);
    }
}
