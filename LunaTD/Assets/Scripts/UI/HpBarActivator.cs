﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBarActivator : MonoBehaviour
{
    [SerializeField] private Hitpoints _hp;
    private Canvas _canvas;


    // Start is called before the first frame update
    void Awake()
    {
        _canvas = gameObject.GetComponent<Canvas>();
        _hp.OnHitpointsChanged += OnHitpointsChanged;
    }

    private void OnHitpointsChanged(float obj)
    {
        if (_hp.currentHP > 0)
        {
            _hp.OnHitpointsChanged -= OnHitpointsChanged;
            _canvas.enabled = true; 
        }
    }
}
