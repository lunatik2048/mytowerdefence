﻿using UnityEngine;

public abstract class WindowBase : MonoBehaviour
{
    [SerializeField] private bool _isActiveOnStart;

    public bool IsActiveOnStart
    {
        get { return _isActiveOnStart; }
    }

    public abstract void Open();

    public abstract void Close();

    public abstract void Initialize();
}
