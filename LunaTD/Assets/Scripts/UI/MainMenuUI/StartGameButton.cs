﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameButton : MonoBehaviour
{
    public void ButtonPressed()
    {
        SceneManager.LoadScene("MainScene");
    }
}
