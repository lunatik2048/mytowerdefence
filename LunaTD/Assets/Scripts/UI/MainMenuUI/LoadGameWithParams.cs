﻿using UnityEngine;

public class LoadGameWithParams : MonoBehaviour
{
    [SerializeField] private string _lvlName;
    [SerializeField] private LevelInfo _info;

    public void Load()
    {
        DataTransformer.SetData(_lvlName, _info);
        SceneLoader.LoadMainScene();
    }
}
