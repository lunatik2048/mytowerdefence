﻿using UnityEngine;

public class CloseWindow : MonoBehaviour
{
    [SerializeField] private WindowBase _window;

    public void Close()
    {
        _window.Close();
    }
}
