﻿using UnityEngine;

public class OpenMainMenu : MonoBehaviour
{
    public void Open()
    {
        SceneLoader.LoadMainMenuScene();
    }
}
