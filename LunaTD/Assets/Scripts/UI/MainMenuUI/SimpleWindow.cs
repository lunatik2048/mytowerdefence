﻿
public class SimpleWindow : WindowBase
{
    public override void Open()
    {
        gameObject.SetActive(true);
    }

    public override void Close()
    {
        gameObject.SetActive(false);
    }

    public override void Initialize()
    {
        
    }
}
