﻿using UnityEngine;

public class WindowsStartVisibility : MonoBehaviour
{
    [SerializeField] private WindowBase[] _windows;

    void Start()
    {
        foreach (var windowBase in _windows)
        {
            windowBase.gameObject.SetActive(windowBase.IsActiveOnStart);
            windowBase.Initialize();
        }
    }
}
