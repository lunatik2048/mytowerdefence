﻿using UnityEngine;
using UnityEngine.UI;

public class GameEndWindowController : SimpleWindow
{
    [SerializeField] private Text _text;

    public override void Initialize()
    {
        ObjectiveController.Instance.GameEnded += InstanceOnGameEnded;
    }

    private void InstanceOnGameEnded(bool won)
    {
        Debug.Log("called");
        if (won)
            _text.text = "You Won! :)";
        else
            _text.text = "You Lost! :(";
        Open();
    }
}
