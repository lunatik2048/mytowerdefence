﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWindow : MonoBehaviour
{
    [SerializeField] private WindowBase _window;

    public void Open()
    {
        _window.Open();
    }
}
