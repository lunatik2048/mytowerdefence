﻿using UnityEngine;

public class TowerBuildSelector : MonoBehaviour
{
    [SerializeField] private GameObject _radius;

    private GameObject _selectedPrefabs;
    private Transform _radTransform;
    private SelectionChanger _changer;
    private BuildTowerButton _currentButton;

    private void Awake()
    {
        _radTransform = _radius.transform;
        _changer = SelectionChanger.Instance;
    }

    public void TryBuilding(GameObject prefab, BuildTowerButton button)
    {
        if (_selectedPrefabs == null)
        {
            _selectedPrefabs = prefab;

            _currentButton = button;
            _currentButton.SelectTower(true);

            ShowRadius();
            _changer.SelectionChanged += OnSelectionChanged;
        }
        else
        {
            if (_selectedPrefabs == prefab)
            {
                Build(prefab);
                _currentButton.SelectTower(false);
                OnSelectionChanged();
            }
            else
            {
                _currentButton.SelectTower(false);

                _currentButton = button;
                _currentButton.SelectTower(true);

                _selectedPrefabs = prefab;
                ShowRadius();
            }
        }
    }

    private void ShowRadius()
    {
        _radius.SetActive(true);
        _radTransform.position = _changer.SelectedGameObject.GetComponent<Buildable>().BuildHeight;
        var rad = _selectedPrefabs.GetComponent<TowerRadius>().Radius;
        _radTransform.localScale = new Vector3(rad, rad, 1);
    }

    private void OnSelectionChanged()
    {
        SelectionChanger.Instance.SelectionChanged -= OnSelectionChanged;
        _currentButton.SelectTower(false);
        _selectedPrefabs = null;
        _radius.SetActive(false);
    }

    private void Build(GameObject prefab)
    {
        MoneyManager.Instance.SpendMoney(prefab.GetComponent<Cost>().MoneyCost);
        SelectionChanger.Instance.SelectedGameObject.GetComponent<Buildable>().BuildTower(prefab);
    }
}
