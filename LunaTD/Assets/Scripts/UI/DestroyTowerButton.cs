﻿using UnityEngine;

public class DestroyTowerButton : MonoBehaviour
{
    public void OnButtonPressed()
    {
        SelectionChanger.Instance.SelectedGameObject.GetComponent<SelectableCell>().DestroyTower();
    }
}
