﻿using UnityEngine;
using UnityEngine.UI;

public class LivesDisplayer : MonoBehaviour
{
    [SerializeField] private Text _livesText;
    [SerializeField] private ObjectiveController _manager;

    void Start()
    {
        _livesText.text = _manager.Lives.ToString();
        _manager.LivesChanged += UpdateUI;
    }

    private void UpdateUI(int value)
    {
        _livesText.text = value.ToString();
    }
}
