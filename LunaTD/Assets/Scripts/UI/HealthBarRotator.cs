﻿using UnityEngine;

public class HealthBarRotator : MonoBehaviour
{
    private GameObject _target = null;

    void Start()
    {
        if (_target == null)
        {
            _target = Camera.main.gameObject;
        }
    }

    void LateUpdate()
    {
        var lookPos = _target.transform.position - transform.position;
        lookPos.z = 6f;
        var rot = Quaternion.LookRotation(lookPos);
        transform.rotation = rot;
    }
}
