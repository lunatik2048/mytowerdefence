﻿using UnityEngine;
using UnityEngine.UI;

public class NextWaveButton : MonoBehaviour
{
    public WaveSpawnController controller;
    public Text text;
    public GameObject nextWaveButton;

    void Start()
    {
        controller.OnWaveComplete += OnWaveComplete;
        text.text = controller.Wave.ToString();
    }

    public void NextWaveButtonPressed()
    {
        controller.StartNextWave();
        nextWaveButton.SetActive(false);
        text.text = controller.Wave.ToString();
    }

    private void OnWaveComplete(int obj)
    {
        nextWaveButton.SetActive(true);
    }
}
