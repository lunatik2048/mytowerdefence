﻿using System.Collections;
using UnityEngine;

public class FollowGameObject : MonoBehaviour
{
    public void Follow(Transform target)
    {
        StartCoroutine(StartFollowing(target, new Vector3(0,0,0)));
    }

    private IEnumerator StartFollowing(Transform t, Vector3 offset)
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            transform.position = t.position + offset;
        }
    }
}
