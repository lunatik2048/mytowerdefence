﻿using DigitalRuby.LightningBolt;
using UnityEngine;

public class ChainEffectController : MonoBehaviour
{
    private ObjectPool _pool;

    void Start()
    {
        _pool = GetComponent<ObjectPool>();
    }

    public void SpawnEffect(GameObject startPos, GameObject endPos)
    {
        var pooled = _pool.GetPooledObject();
        var bolt = pooled.GetComponent<LightningBoltScript>();
        bolt.StartPosition = startPos.transform.position;
        bolt.EndPosition = endPos.transform.position;
        pooled.SetActive(true);
        bolt.Trigger();
        bolt.GetComponent<AfterTimeDestruction>().DoEffect();
    }
}
