﻿using System.Collections;
using UnityEngine;

public class AnimatorDeathHandler : MonsterDestructionController
{
    [SerializeField] private Animator _anim;

    protected override IEnumerator StartDeathAnimation()
    {
        _anim.SetBool(Constants.DeadAnim, true);
        yield return new WaitForEndOfFrame();
        var l = _anim.GetCurrentAnimatorStateInfo(0).length;
        yield return new WaitForSeconds(l);
        OnDestruction();
    }
}
