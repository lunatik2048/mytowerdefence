﻿using UnityEngine;

public class TowerShootingAnimation : MonoBehaviour, IAttacker
{
    [SerializeField] private Animator _anim;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Attack(GameObject g, StatisticsCounter counter)
    {
        _anim.SetTrigger("Fire");
    }
}
