﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterTimeDestruction : MonoBehaviour
{
    [SerializeField] private float _timer;

    private IDestructable _destr;
    protected WaitForSeconds _waitTimer;

    protected void Awake()
    {
        _destr = GetComponent<IDestructable>();
        _waitTimer = new WaitForSeconds(_timer);
    }

    public void DoEffect()
    {
        StartCoroutine(WaitTillAnimPlayed());
    }

    private IEnumerator WaitTillAnimPlayed()
    {
        yield return _waitTimer;
        _destr.Destroy();
    }
}
