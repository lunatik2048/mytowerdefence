﻿using UnityEngine;

[RequireComponent(typeof(PooledObject))]
public class ParticleSystemDestruction : AfterTimeDestruction
{
    void Awake()
    {
        base.Awake();
        var ps = GetComponent<ParticleSystem>();
        _waitTimer = new WaitForSeconds(ps.main.duration);
    }
}
