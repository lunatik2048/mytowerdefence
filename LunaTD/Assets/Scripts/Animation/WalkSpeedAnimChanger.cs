﻿using UnityEngine;

public class WalkSpeedAnimChanger : MonoBehaviour
{
    [SerializeField] private float _baseSpeedModifier;
    [SerializeField] private Animator _animator;

    private NavMonsterMovement _nav;

    void Awake()
    {
        _nav = GetComponent<NavMonsterMovement>();
        _nav.SpeedChanged += SpeedChanged;
    }

    void Start()
    {
        SpeedChanged(_nav.CurrentUnitSpeed);
    }

    public void SpeedChanged(float speed)
    {
        _animator.SetFloat(Constants.WalkSpeedAnim, _baseSpeedModifier * speed);
    }
}
