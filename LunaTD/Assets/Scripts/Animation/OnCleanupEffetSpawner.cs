﻿using UnityEngine;

public class OnCleanupEffetSpawner : MonoBehaviour, ICleanUp
{
    private ObjectPool _pool;

    void Awake()
    {
        _pool = GetComponent<ObjectPool>();
    }

    public void CleanUp()
    {
        var ps = _pool.GetPooledObject();
        ps.transform.position = transform.position;
        ps.SetActive(true);
        ps.GetComponent<ParticleSystemDestruction>().DoEffect();
    }
}
