﻿using UnityEngine;

public class TowerShootingEffect : MonoBehaviour, IAttacker
{
    [SerializeField] private ParticleSystem _ps;
    private Transform _tr;

    void Awake()
    {
        _tr = _ps.transform;
    }

    public void Attack(GameObject g, StatisticsCounter counter)
    {
        _tr.LookAt(g.transform);
        _ps.Play();
    }
}
