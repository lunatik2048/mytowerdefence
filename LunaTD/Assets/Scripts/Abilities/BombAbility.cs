﻿using UnityEngine;

public class BombAbility : AoeObjectSpawner, ISelectable
{
    private bool _isSelected;
    private StatisticsCounter _counter;

    public bool IsSelected
    {
        get { return _isSelected; }
        set { _isSelected = value; }
    }

    void Awake()
    {
        base.Awake();
        _counter = GetComponent<StatisticsCounter>();
    }

    private void SelectionChanged(bool isSelected)
    {
        
    }

    public void ActivateBomb(Vector3 position)
    {
        base.Spawn(position, Quaternion.identity, _counter);
    }
}
