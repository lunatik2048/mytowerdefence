﻿
using System.Collections;
using UnityEngine;

public class AOEInstantDamageObject : AOEDestructableObject
{
    public void Attack()
    {
        StartCoroutine(Destr());
    }

    IEnumerator Destr()
    {
        yield return new WaitForSeconds(0.1f);
        DealDamageAndDestroy();
    }
}
