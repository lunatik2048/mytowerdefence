﻿using UnityEngine;

public class FreezeAbility : MonoBehaviour, IAbility
{

    public void Activate()
    {
        var monsters = WaveSpawnController.Instance.Monsters;

        for (int i = 0; i < monsters.Count; i++)
        {
            monsters[i].GetComponent<DebuffManager>().AddDebuff(null, new FreezeDebuff(5), DebuffType.Freeze);
        }
    }
}
