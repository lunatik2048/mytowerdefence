﻿using System;
using UnityEngine;
using UnityEngine.UIElements;

public class ObjectiveController : MonoBehaviour
{
    private int _lives;

    public static ObjectiveController Instance;
    public event Action<bool> GameEnded; 

    void Awake()
    {
        Instance = this;
    }

    public int Lives
    {
        get { return _lives;}
        set
        {
            _lives = value;
            LivesChanged?.Invoke(Lives);
        }
    }

    public Action<int> LivesChanged;

    public void OnEndpointReached(GameObject go)
    {
        Lives -= go.GetComponent<LivesSubstractor>().Lives;
        if (Lives <= 0)
        {
            HandleGameEnd(false);
        }
    }

    public void LastWaveComplete()
    {
        HandleGameEnd(true);
    }

    private void HandleGameEnd(bool won)
    {
        TimeManager.PauseGame();
        GameEnded?.Invoke(won);
    }
}
