﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawnController : MonoBehaviour
{
    [SerializeField] private MonsterCharacteristics[] _monsterCharacteristics;

    private int _wave;
    private int _enemiesLeftToSpawn;
    private int _numOfEnemies;

    private ObjectiveController _objectiveController;
    private MoneyManager _moneyManager;
    private LevelInfo _level;
    private float _timeInterval;
    private Vector3 _startPos;
    private Vector3 _endPos;

    public Action<int> OnWaveComplete;
    public static WaveSpawnController Instance;

    public List<GameObject> Monsters { get; private set; }

    public int Wave
    {
        get => _wave;
        private set
        {
            _wave = value;
        }
    }

    void Awake()
    {
        Instance = this;
        Monsters = new List<GameObject>();
        _wave = 0;
        _objectiveController = GetComponent<ObjectiveController>();
    }

    public void InitializeLevel(LevelInfo level, float timeInterval, Vector3 startPos, Vector3 endPos, MoneyManager moneyManager)
    {
        _level = level;
        _timeInterval = timeInterval;
        _startPos = startPos;
        _endPos = endPos;
        _objectiveController.Lives = level.lives;
        _moneyManager = moneyManager;
    }

    public IEnumerator SpawnWave()
    {
        var currentWave = _level.waves[_wave - 1];

        foreach (var packN in currentWave.packs)
        {
            var num = GetPrefabById(packN.id).numOfMinions;
            _enemiesLeftToSpawn += packN.num;
            _numOfEnemies += packN.num * num;
        }

        var delay = new WaitForSeconds(_timeInterval);
        int pack = 0;
        int spawnedFromPack = 0;
        var pos = new Vector3(_startPos.x, 0.25f, _startPos.z);
        while (_enemiesLeftToSpawn != 0)
        {
            if (spawnedFromPack == currentWave.packs[pack].num)
            {
                pack++;
                spawnedFromPack = 0;
            }

            SpawnMonster(currentWave.packs[pack].id, pos);

            _enemiesLeftToSpawn--;
            spawnedFromPack++;
            yield return delay;
        }
    }

    public GameObject SpawnMonster(MonstersId id, Vector3 pos)
    {
        var monsterStuff = GetPrefabById(id);

        var go = Instantiate(monsterStuff.prefab, pos, Quaternion.identity, ParentContainer.Instance.MonstersParent);
        var movement = go.GetComponent<NavMonsterMovement>();
        movement.SetDestination(_endPos, monsterStuff.movementSpeed);

        var hp = go.GetComponent<Hitpoints>();
        hp.maxHP = monsterStuff.maxHitpoints;
        hp.currentHP = monsterStuff.maxHitpoints;
        hp.OnMonsterDeath += OnMonsterDeath;

        var port = go.GetComponent<MonsterPortalController>();
        port.OnMonsterReachedPortal += OnMonsterReachedPortal;
        port.OnMonsterReachedPortal += _objectiveController.OnEndpointReached;
        port.MonsSpawned = monsterStuff.numOfMinions;

        var sub = go.GetComponent<LivesSubstractor>();
        sub.Lives = monsterStuff.liveReduction;

        var money = go.GetComponent<MoneyBringer>();
        money.Setup(monsterStuff.money, _moneyManager);
        Monsters.Add(go);
        return go;
    }

    private void OnMonsterReachedPortal(GameObject obj)
    {
        CheckForCompletion(obj.GetComponent<MonsterPortalController>().MonsSpawned, obj);
    }

    public void StartNextWave()
    {
        _wave++;
        StartCoroutine(SpawnWave());
    }

    private void OnMonsterDeath(GameObject g)
    {
        CheckForCompletion(1, g);
    }

    private void CheckForCompletion(int n, GameObject g)
    {
        Monsters.Remove(g);
        _numOfEnemies -= n;
        if (_numOfEnemies + _enemiesLeftToSpawn == 0)
        {
            var count = _level.waves.Length;
            if (count == _wave)
            {
                Debug.Log("Win");
                _objectiveController.LastWaveComplete();
                return;
            }
            Debug.Log("Wave " + _wave + " complete");
            OnWaveComplete(_wave);
        }
    }

    private MonsterCharacteristics GetPrefabById(MonstersId id)
    {
        for (int i = 0; i < _monsterCharacteristics.Length; i++)
        {
            if (id == _monsterCharacteristics[i].id)
                return _monsterCharacteristics[i];
        }
        return null;
    }
}
