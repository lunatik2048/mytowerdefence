﻿using UnityEngine;

public class ProxShield : MonoBehaviour
{
    private Hitpoints _hitpoins;

    void Awake()
    {
        _hitpoins = GetComponent<Hitpoints>();
    }

    void OnTriggerEnter(Collider c)
    {
        var destr = c.GetComponent<DestructionController>();

        _hitpoins.DealDamage(c.GetComponent<DamageComponent>().Damage, c.GetComponent<SourceContainer>().Counter);

        destr.CleanUpAndDestroy();
    }
}
