﻿using System.Collections;
using UnityEngine;

public class SplitedMonster : MonoBehaviour
{
    [SerializeField] private float _timer;

    public void WaitForSplit()
    {
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        var move = GetComponent<NavMonsterMovement>();
        move.Stop();
        yield return new WaitForSeconds(_timer);
        move.ContinueMoving();
    }
}
