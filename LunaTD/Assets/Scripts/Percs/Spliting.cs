﻿using UnityEngine;

public class Spliting : MonoBehaviour, ICleanUp
{
    [SerializeField] private MonstersId _id;

    public void CleanUp()
    {
        if (GetComponent<Hitpoints>().currentHP <= 0)
        {
            var distance = GetComponent<NavMonsterMovement>().DistanceTraveled;
            var spl1 = WaveSpawnController.Instance.SpawnMonster(_id, transform.position);
            spl1.GetComponent<NavMonsterMovement>().DistanceTraveled = distance;
            var spl2 = WaveSpawnController.Instance.SpawnMonster(_id, transform.position);
            spl2.GetComponent<SplitedMonster>().WaitForSplit();
            spl2.GetComponent<NavMonsterMovement>().DistanceTraveled = distance;
        }
    }
}
