﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Berserk : MonoBehaviour
{
    [SerializeField] private float _maxMulti;

    private Hitpoints _hp;
    private NavMonsterMovement _movement;
    private float _prevHP;

    void Start()
    {
        _hp = GetComponent<Hitpoints>();
        _movement = GetComponent<NavMonsterMovement>();
        _prevHP = 1f;
        _hp.OnHitpointsChanged += OnHitpointsChanged;
    }

    private void OnHitpointsChanged(float obj)
    {
        var percentage = _hp.GetPercentage();
        if (Mathf.Abs( (_maxMulti + 1 - _prevHP) / _maxMulti -  percentage) > 0.05f)
        {
            var speedM = 1f + (1f - percentage) * _maxMulti;
            var diff = speedM / _prevHP;
            _movement.ChangeIncMsMulti(diff);
            _prevHP = speedM;
        }
    }
}
