﻿using System;
using UnityEngine;

public class SelectionChanger : MonoBehaviour
{
    private ISelectable _selectedObject;

    public Selection Default;
    public Selection Ability;

    public Selection Selection { get; set; }
    public GameObject SelectedGameObject { get; private set; }

    public event Action SelectionChanged;

    public static SelectionChanger Instance;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Selection = Default;
    }

    void Update()
    {
        Selection.Check();
    }

    public void SetSelected(GameObject newSelectedObj)
    {
        if (newSelectedObj != SelectedGameObject)
        {
            SelectionChanged?.Invoke();
        }

        if (_selectedObject != null)
        {
            _selectedObject.IsSelected = false;
        }

        if (newSelectedObj != null)
        {
            _selectedObject = newSelectedObj.GetComponent<ISelectable>();
            SelectedGameObject = newSelectedObj;
            _selectedObject.IsSelected = true; 
        }
    }
}
