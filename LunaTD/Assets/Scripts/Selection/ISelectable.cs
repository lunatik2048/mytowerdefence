﻿using JetBrains.Annotations;

public interface ISelectable
{
	bool IsSelected { get; set; }
}
