﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SelectionController : Selection
{
    private ISelectable _selectedObject;
    private int _layerMask;
    private SelectionChanger _selectionChanger;

    public bool Relocate { get;  set; }

    public static SelectionController Instance;

    void Awake()
    {
        Instance = this;
        _layerMask = LayerMask.GetMask(Constants.SelectableLayerName);
    }

    void Start()
    {
        _selectionChanger = SelectionChanger.Instance;
    }

    public void SetSelected(GameObject newSelectedObj)
    {
        if (Relocate)
        {
            var newBuildable = newSelectedObj.GetComponent<Buildable>();

            if (newBuildable.Tower == null)
            {
                var buildable = _selectionChanger.SelectedGameObject.GetComponent<Buildable>();
                RelocateTower(buildable.Tower, buildable, newBuildable); 
            }
            Relocate = false;
        }
        _selectionChanger.SetSelected(newSelectedObj);
    }

    public void RelocateTower(GameObject tower, Buildable from, Buildable to)
    {
        var tr = to.transform;
        var newPos = new Vector3(tr.position.x, tower.transform.position.y, tr.position.z);
        tower.transform.position = newPos;
        from.Tower = null;
        to.Tower = tower;
        tower.transform.parent = to.gameObject.transform;
    }

    public override void Check()
    {
        if (Input.GetButtonDown(InputKeys.Fire1))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100, _layerMask))
            {
                var selected = hit.collider.gameObject;
                if (selected != null)
                {
                    SetSelected(selected);
                }
            }
        }
    }
}
