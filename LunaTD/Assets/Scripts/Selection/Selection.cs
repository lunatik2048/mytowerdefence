﻿using UnityEngine;

public abstract class Selection : MonoBehaviour
{
    public abstract void Check();
}
