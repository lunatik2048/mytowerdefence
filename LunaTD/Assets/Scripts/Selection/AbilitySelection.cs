﻿using UnityEngine;
using UnityEngine.EventSystems;

public class AbilitySelection : Selection
{
    [SerializeField] private BombAbility _bombAbility;
    [SerializeField] private GameObject _radiusDisp;

    private int _layerMask;

    void Awake()
    {
        _layerMask = LayerMask.GetMask(Constants.PathLayerName, Constants.SelectableLayerName);
    }

    public override void Check()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            if (_radiusDisp.activeInHierarchy)
            {
                _radiusDisp.SetActive(false); 
            }
            return;
        }
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, _layerMask))
        {
            var selected = hit.collider.gameObject;
            if (selected != null)
            {
                if (!_radiusDisp.activeInHierarchy)
                {
                    _radiusDisp.SetActive(true);
                }

                _radiusDisp.transform.position = hit.point;
                if (Input.GetButtonDown(InputKeys.Fire1))
                {
                    _radiusDisp.SetActive(false);
                    Bomb(hit.point);
                }
            }
        }
        else
        {
            if (_radiusDisp.activeInHierarchy)
            {
                _radiusDisp.SetActive(false);
            }
        }
    }

    private void Bomb(Vector3 point)
    {
        _bombAbility.ActivateBomb(point);
        SelectionChanger.Instance.SetSelected(null);
        SelectionChanger.Instance.Selection = SelectionChanger.Instance.Default;
    }

    public void BombSelected()
    {
        SelectionChanger.Instance.Selection = SelectionChanger.Instance.Ability;
        SelectionChanger.Instance.SetSelected(_bombAbility.gameObject);
    }
}
