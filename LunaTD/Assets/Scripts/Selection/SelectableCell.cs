﻿using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(Buildable))]
public class SelectableCell : MonoBehaviour, ISelectable
{
    [SerializeField] private GameObject _selectionMarker;
    private Buildable _towerInfo;
    private bool _isSelected;

    public bool IsSelected
    {
        get { return _isSelected; }
        set
        {
            _isSelected = value;
            OnSelectionChanged();
        }
    }

    void Start()
    {
        _towerInfo = GetComponent<Buildable>();
        _towerInfo.SellStateChanged += SellStateChanged;
    }

    private void SellStateChanged(Buildable obj)
    {
        OnSelectionChanged();
    }

    private void OnSelectionChanged()
    {
        if (_isSelected)
        {
            GameInitializer.TowerCreationInterface.SetActive( !_towerInfo.HasTower);

            if (_towerInfo.HasTower)
            {
                var cd = _towerInfo.Tower.GetComponent<CooldownAttackController>() != null ? _towerInfo.Tower.GetComponent<CooldownAttackController>().Cooldown : 0;
                var rad = _towerInfo.Tower.GetComponent<SphereCollider>().radius;
                var dmg = _towerInfo.Tower.GetComponent<DamageComponent>() != null ? _towerInfo.Tower.GetComponent<DamageComponent>().Damage : 0;
                var info = _towerInfo.Tower.GetComponent<StatisticsCounter>();
                _towerInfo.Tower.GetComponent<TowerRadius>().ShowRadius();
                GameInitializer.TowerInfoInterface.GetComponent<TowerInfoDisplayer>().Show(dmg, cd, rad, info);
            }
            else
            {
                GameInitializer.TowerInfoInterface.GetComponent<TowerInfoDisplayer>().Hide();
            }
        }

        if (!_isSelected)
        {
            if (_towerInfo.HasTower)
            {
                GameInitializer.TowerInfoInterface.GetComponent<TowerInfoDisplayer>().Hide();
                _towerInfo.Tower.GetComponent<TowerRadius>().HideRadius();
            }
            else
            {
                GameInitializer.TowerCreationInterface.SetActive(false);
            }
        }

        _selectionMarker.SetActive(_isSelected);
    }

    public void DestroyTower()
    {
        Destroy(_towerInfo.Tower);
        _towerInfo.Tower = null;
        OnSelectionChanged();
    }
}
