﻿using UnityEngine;

public class SlowDebuffApplier : MonoBehaviour, IAttacker
{
    [SerializeField] private float _force;
    [SerializeField] private float _timer;

    private SlowingTimedDebuff _debuffToTry;

    void Awake()
    {
        _debuffToTry = new SlowingTimedDebuff(_timer, _force);
    }

    public void Attack(GameObject g, StatisticsCounter counter)
    {
        var dManager = g.GetComponent<DebuffManager>();
        var res = dManager.TryRefreshDebuff(counter.gameObject, DebuffType.Slow, _debuffToTry);

        if (!res)
        {
            var slow = new SlowingTimedDebuff(_timer, _force);
            g.GetComponent<DebuffManager>().AddDebuff(counter.gameObject, slow, DebuffType.Slow);
        }
    }
}
