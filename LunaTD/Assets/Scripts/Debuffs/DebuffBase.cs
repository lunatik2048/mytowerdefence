﻿using System;
using UnityEngine;

public abstract class DebuffBase
{
    public GameObject Source { get; protected set; }

    public DebuffType Type { get; protected set; }

    public GameObject Target { get; set; }

    public int Stacks { get; protected set; }

    public int MaxStacks { get; protected set; }

    public bool CanStack { get; protected set; }

    public event Action OnApplication;
    public event Action<DebuffBase> OnExprire;

    public virtual void OnDebuffApplied(GameObject target, GameObject source, DebuffManager manager, DebuffType debuffType)
    {
        Target = target;
        Source = source;
        Type = debuffType;
        Stacks = 1;
        OnApplication?.Invoke();
    }

    public virtual void ActivateStacking(int maxStacks)
    {
        MaxStacks = maxStacks;
    }

    public void AddStack()
    {
        if (Stacks >= MaxStacks)
            return;
        Stacks += 1;
    }

    public virtual void OnDebuffReapplied(DebuffBase debuff)
    {
        AddStack();
    }

    public virtual void OnDebuffExpired()
    {
        OnExprire?.Invoke(this);
    }
}
