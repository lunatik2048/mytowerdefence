﻿using System.Collections.Generic;
using UnityEngine;

public class DebuffParticles
{
    private Dictionary<DebuffType, GameObject> _particles;
    private DebuffManager _debuffManager;
    private Transform _followObj;

    public DebuffParticles(DebuffManager debuffManager)
    {
        _particles = new Dictionary<DebuffType, GameObject>();
        _debuffManager = debuffManager;
        _followObj = _debuffManager.gameObject.transform;
    }

    public void TryAddingParticleSystem(ObjectPool pool, DebuffType type, DebuffBase debuff)
    {
        if (!_debuffManager.Cleaned)
        {
            if (!_particles.ContainsKey(type))
            {
                var ps = pool.GetPooledObject();
                ps.SetActive(true);
                var follow = ps.GetComponent<FollowGameObject>();
                if (follow)
                {
                    follow.Follow(_followObj);
                }
                debuff.OnExprire += DebuffOnExprire;
                _particles.Add(type, ps);
            } 
        }
    }

    private void DebuffOnExprire(DebuffBase obj)
    {
        var newDebuff = _debuffManager.FindDebuffOfType(obj.Type);
        GameObject ps;
        if (_particles.TryGetValue(obj.Type, out ps))
        {
            if (newDebuff != null)
            {
                newDebuff.OnExprire += DebuffOnExprire;
                ps.GetComponent<FollowGameObject>().Follow(_followObj);
            }
            else
            {
                _particles.Remove(obj.Type);
                ps.GetComponent<IDestructable>().Destroy();
            }
        }
        else
        {
            Debug.Log("Unexpected");
        }
    }
}
