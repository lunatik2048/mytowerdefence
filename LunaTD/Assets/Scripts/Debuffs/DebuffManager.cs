﻿using System.Collections.Generic;
using UnityEngine;

public class DebuffManager : MonoBehaviour, ICleanUp
{
    private List<DebuffBase> _debuffs;
    public DebuffParticles Particles { get; private set; }
    public bool Cleaned { get; private set; }

    void Start()
    {
        _debuffs = new List<DebuffBase>();
        Particles = new DebuffParticles(this);
    }

    public void AddDebuff(GameObject source, DebuffBase debuff, DebuffType type)
    {
        if (!Cleaned)
        {
            var d = FindDebuff(source, type);
            if (d != null)
            {
                d.OnDebuffReapplied(debuff);
                return;
            }
            _debuffs.Add(debuff);
            debuff.OnDebuffApplied(gameObject, source, this, type); 
        }
    }

    public void RemoveDebuff(GameObject source, DebuffType type)
    {
        var debuff = FindDebuff(source, type);

        if (debuff == null)
        {
            return;
        }

        _debuffs.Remove(debuff);
        debuff.OnDebuffExpired();
    }

    public bool TryRefreshDebuff(GameObject source, DebuffType type, DebuffBase deb)
    {
        var d = FindDebuff(source, type);
        if (d == null)
        {
            return false;
        }
        else
        {
            d.OnDebuffReapplied(d);
            return true;
        }
    }

    private DebuffBase FindDebuff(GameObject source, DebuffType type)
    {
        for (int i = 0; i < _debuffs.Count; i++)
        {
            var d = _debuffs[i];
            if (d.Source == source && d.Type == type)
            {
                return d;
            }
        }
        return null;
    }

    public DebuffBase FindDebuffOfType(DebuffType type)
    {
        for (int i = 0; i < _debuffs.Count; i++)
        {
            var d = _debuffs[i];
            if (d.Type == type)
            {
                return d;
            }
        }
        return null;
    }

    public List<DebuffBase> FindAllDebuffsOfType(DebuffType type)
    {
        var res = new List<DebuffBase>();
        for (int i = 0; i < _debuffs.Count; i++)
        {
            var d = _debuffs[i];
            if (d.Type == type)
            {
                res.Add(d);
            }
        }
        return res;
    }

    public void CleanUp()
    {
        Cleaned = true;
        for (int i = _debuffs.Count - 1; i >= 0; i--)
        {
            var deb =_debuffs[i];
            _debuffs.RemoveAt(i);
            deb.OnDebuffExpired();
        }
    }
}
