﻿using UnityEngine;

public class AreaSlowDebuffApplier : AreaDebuffer
{
    [SerializeField] private float _force;

    protected override void MonsterLeftRadius(GameObject obj)
    {
        obj.GetComponent<DebuffManager>().RemoveDebuff(gameObject, DebuffType.Slow);
    }

    protected override void MonsterEnteredRadius(GameObject obj)
    {
        obj.GetComponent<DebuffManager>().AddDebuff(gameObject, new SlowingAreaDebuff(_force), DebuffType.Slow);
    }

    protected void OnDisable()
    {
        base.OnDisable();
        foreach (var monstersInRadius in _areaAttackController.MonstersInRadius)
        {
            if (monstersInRadius != null)
                monstersInRadius.GetComponent<DebuffManager>().RemoveDebuff(gameObject, DebuffType.Slow);
        }
    }
}
