﻿using UnityEngine;

public class SlowingAreaDebuff : DebuffBase, ISlowDebuff
{
    private float _force;

    public SlowingAreaDebuff(float slowforce)
    {
        _force = slowforce;
    }

    public override void OnDebuffApplied(GameObject target, GameObject source, DebuffManager manager, DebuffType type)
    {
        base.OnDebuffApplied(target, source, manager, type);
        Target.GetComponent<NavMonsterMovement>().ApplySlowDebuff(this);
    }

    public override void OnDebuffExpired()
    {
        base.OnDebuffExpired();
        Target.GetComponent<NavMonsterMovement>().RemoveSlowDebuff(this);
    }

    public float GetSlowForce()
    {
        return _force;
    }
}
