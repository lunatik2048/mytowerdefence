﻿using UnityEngine;

public class SlowingTimedDebuff : TimeBasedDebuff, ISlowDebuff
{
    private float _force;

    public SlowingTimedDebuff(float time, float force) : base(time)
    {
        _force = force;
    }

    public override void OnDebuffApplied(GameObject target, GameObject source, DebuffManager manager, DebuffType type)
    {
        base.OnDebuffApplied(target, source, manager, type);
        Target.GetComponent<NavMonsterMovement>().ApplySlowDebuff(this);
    }

    public override void OnDebuffExpired()
    {
        base.OnDebuffExpired();
        Target.GetComponent<NavMonsterMovement>().RemoveSlowDebuff(this);
    }

    public float GetSlowForce()
    {
        return _force;
    }
}
