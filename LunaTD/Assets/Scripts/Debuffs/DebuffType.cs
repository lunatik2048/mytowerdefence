﻿using UnityEngine;

public enum DebuffType
{
    Slow,
    DoT,
    Freeze
}
