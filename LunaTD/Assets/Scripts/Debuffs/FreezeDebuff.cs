﻿using UnityEngine;

public class FreezeDebuff : TimeBasedDebuff
{
    private NavMonsterMovement _movement;

    public FreezeDebuff(float maxTime) : base(maxTime)
    {

    }

    public override void OnDebuffApplied(GameObject target, GameObject source, DebuffManager manager, DebuffType type)
    {
        base.OnDebuffApplied(target, source, manager, type);
        _movement = target.GetComponent<NavMonsterMovement>();
        _movement.Stop();
    }

    public override void OnDebuffExpired()
    {
        _movement.ContinueMoving();
        base.OnDebuffExpired();
    }
}
