﻿using UnityEngine;

public class DamageTickingDebuffApplier : MonoBehaviour, IAttacker
{
    [SerializeField] private float _timer;
    [SerializeField] private float _damageOverTime;
    [SerializeField] private float _tick;

    private ObjectPool _pool;
    private DotDebuff _debuffToTry;

    void Awake()
    {
        _pool = GetComponent<ObjectPool>();
        _debuffToTry = new DotDebuff(_timer, _tick, _damageOverTime);
    }

    public void Attack(GameObject g, StatisticsCounter counter)
    {
        var source = counter.gameObject;
        var dManager = g.GetComponent<DebuffManager>();
        var res = dManager.TryRefreshDebuff(source, DebuffType.DoT, _debuffToTry);

        if (!res)
        {
            var debManager = g.GetComponent<DebuffManager>();
            var debuff = new DotDebuff(_timer, _tick, _damageOverTime);
            g.GetComponent<DebuffManager>().AddDebuff(source, debuff, DebuffType.DoT);
            debManager.Particles.TryAddingParticleSystem(_pool, DebuffType.DoT, debuff);
        }
    }
}
