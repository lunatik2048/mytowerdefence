﻿using System;
using UnityEngine;

public class DotDebuff : TickingDebuff
{
    private Hitpoints _hp;
    private float _damage;
    private StatisticsCounter _counter;

    public DotDebuff(float maxTime, float tickTimer, float damage) : base(maxTime, tickTimer)
    {
        _damage = damage;
    }

    public override void OnDebuffApplied(GameObject target, GameObject source, DebuffManager manager, DebuffType type)
    {
        base.OnDebuffApplied(target, source, manager, type);
        _hp = Target.GetComponent<Hitpoints>();
        _counter = Source.GetComponent<StatisticsCounter>();
    }

    protected override void Tick()
    {
        _hp.DealDamage((float)(_damage), _counter);
    }
}
