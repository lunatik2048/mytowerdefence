﻿using System.Collections;
using UnityEngine;

public abstract class TickingDebuff : TimeBasedDebuff
{
    protected float _tickTimer;

    public TickingDebuff(float maxTime, float tickTimer) : base(maxTime)
    {
        _tickTimer = tickTimer;
    }

    public override void OnDebuffApplied(GameObject target, GameObject source, DebuffManager manager, DebuffType type)
    {
        base.OnDebuffApplied(target, source, manager, type);
        target.GetComponent<Hitpoints>().StartCoroutine(TickCoroutine());
    }

    private IEnumerator TickCoroutine()
    {
        var wait = new WaitForSeconds(_tickTimer);
        while (_timeLeft > 0)
        {
            yield return wait;
            Tick();
        }
    }

    protected abstract void Tick();
}
