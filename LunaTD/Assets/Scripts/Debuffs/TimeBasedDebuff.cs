﻿using System.Collections;
using UnityEngine;

public abstract class TimeBasedDebuff : DebuffBase
{
    public float MaxTime { get; private set; }
    private DebuffManager _manager;
    protected float _timeLeft;

    public TimeBasedDebuff(float maxTime)
    {
        MaxTime = maxTime;
    }

    public override void OnDebuffApplied(GameObject target, GameObject source, DebuffManager manager, DebuffType type)
    {
        base.OnDebuffApplied(target, source, manager, type);
        _manager = manager;
        _timeLeft = MaxTime;
        target.GetComponent<Hitpoints>().StartCoroutine(StartTimer());
    }

    private IEnumerator StartTimer()
    {
        while (_timeLeft > 0)
        {
            yield return null;
            _timeLeft -= Time.deltaTime;
        }
        _manager.RemoveDebuff(Source, Type);
    }

    public override void OnDebuffReapplied(DebuffBase debuff)
    {
        base.OnDebuffReapplied(debuff);

        var timeDebuff = debuff as TimeBasedDebuff;
        if (timeDebuff.MaxTime > _timeLeft)
            _timeLeft = timeDebuff.MaxTime;
    }

    public override void OnDebuffExpired()
    {
        base.OnDebuffExpired();
        _timeLeft = 0;
    }
}
