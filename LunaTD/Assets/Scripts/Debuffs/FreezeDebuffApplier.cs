﻿using UnityEngine;

public class FreezeDebuffApplier : MonoBehaviour, IAttacker
{
    [SerializeField] private float _timer;

    private ObjectPool _pool;
    private FreezeDebuff _debuffToTry;

    void Awake()
    {
        _pool = GetComponent<ObjectPool>();
        _debuffToTry = new FreezeDebuff(_timer);
    }

    public void Attack(GameObject g, StatisticsCounter counter)
    {
        var dManager = g.GetComponent<DebuffManager>();
        var res = dManager.TryRefreshDebuff(null, DebuffType.Freeze, _debuffToTry);

        if (!res)
        {
            var debManager = g.GetComponent<DebuffManager>();
            var debuff = new FreezeDebuff(_timer);
            g.GetComponent<DebuffManager>().AddDebuff(null, debuff, DebuffType.Freeze);
            debManager.Particles.TryAddingParticleSystem(_pool, DebuffType.Freeze, debuff);
        }
    }
}
