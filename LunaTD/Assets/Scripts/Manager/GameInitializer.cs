﻿using UnityEngine;

public class GameInitializer : MonoBehaviour
{
    //testing interface
    public GameObject towerCreationInterface;
    public static GameObject TowerCreationInterface;
    public GameObject towerInfoInterface;
    public static GameObject TowerInfoInterface;
    //testing interface

    [SerializeField] private string _lvlName;
    [SerializeField] private LevelInfo _lvl;
    [SerializeField] private CameraHandler _cam;

	void Awake()
    {
        if (!string.IsNullOrEmpty(DataTransformer.LvlName))
        {
            _lvlName = DataTransformer.LvlName;
            _lvl = DataTransformer.Lvl;
        }

        TowerInfoInterface = towerInfoInterface;
        TowerCreationInterface = towerCreationInterface;
    }

	void Start()
    {
        var grid = GetComponent<GridController>();
        grid.CreateGrid(_lvlName);

        _cam.SetUp(grid.Cells.GetLength(0), grid.Cells.GetLength(1));

        var startP = GridController.GetWorldCoordinates(grid.StartPoint);
        var endP = GridController.GetWorldCoordinates(grid.FinishPoint);
        endP.y = Constants.FlourHeight;

        MoneyManager.Instance.Initialize(_lvl.startingMoney);
        GetComponent<WaveSpawnController>().InitializeLevel(_lvl, 0.5f, startP, endP, MoneyManager.Instance);
	}
}
