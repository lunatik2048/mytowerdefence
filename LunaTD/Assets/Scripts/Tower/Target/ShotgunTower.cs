﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunTower : AoeObjectSpawner, IAttacker
{
    private Transform _transform;
    private StatisticsCounter _stats;

    void Awake()
    {
        base.Awake();
        _transform = transform;
        _stats = GetComponent<StatisticsCounter>();
    }

    public void Attack(GameObject g, StatisticsCounter counter)
    {
        var sourcePos = _transform.position;
        var targetPos = g.transform.position;

        var pos = sourcePos + (targetPos - sourcePos).normalized * 1.25f;
        pos.y = Constants.FlourHeight;

        var lookPos = targetPos - pos;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);

        base.Spawn(pos, rotation, _stats);
    }
}
