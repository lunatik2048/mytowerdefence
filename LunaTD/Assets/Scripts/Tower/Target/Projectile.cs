﻿using UnityEngine;

public class Projectile : MonoBehaviour, ITargetWaiter
{
    [SerializeField] private float _movespeed;
    private IAttacker _attacker;
    private SourceContainer _source;
    private Hitpoints _hp;
    private DestructionController _dest;

    public Transform Target { get; private set; }

    void Start()
    {
        _attacker = GetComponent<IAttacker>();
        _source = GetComponent<SourceContainer>();
        _dest = GetComponent<DestructionController>();
    }

    void FixedUpdate()
    {
        if (Target == null || _hp.IsDeathCalled)
        {
            _dest.CleanUpAndDestroy();
            return;
        }

        transform.LookAt(Target);
        transform.Translate(Vector3.forward * Time.deltaTime * _movespeed);

        if (Vector3.Distance(transform.position, Target.position) < Constants.DestinationReachThreshold)
        {
            Reached();
        }
    }

    void Reached()
    {
        _attacker.Attack(Target.gameObject.gameObject, _source.Counter);
        _dest.CleanUpAndDestroy();
    }

    public void SetTarget(GameObject target)
    {
        Target = target.transform;
        _hp = Target.GetComponent<Hitpoints>();
    }
}
