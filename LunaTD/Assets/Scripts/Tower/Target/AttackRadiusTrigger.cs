﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AttackRadiusTrigger : MonoBehaviour, ICleanUp
{
    public List<GameObject> MonstersInRadius { get; private set; }
    public Action<GameObject> MonsterLeftRadius;
    public Action<GameObject> MonsterEnteredRadius;

    void Awake()
    {
        MonstersInRadius = new List<GameObject>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == Constants.MonsterLayer)
        {
            MonstersInRadius.Add(other.gameObject);
            var hp = other.gameObject.GetComponent<Hitpoints>();
            hp.OnMonsterDeath += OnMonsterDeath;
            other.gameObject.GetComponent<MonsterPortalController>().OnMonsterReachedPortal += OnMonsterDeath;
            MonsterEnteredRadius?.Invoke(other.gameObject); 
        }
    }

    private void OnMonsterDeath(GameObject g)
    {
        OnMonsterLeftRadius(g);
    }

    void OnTriggerExit(Collider other)
    {
        OnMonsterLeftRadius(other.gameObject);
    }

    private void OnMonsterLeftRadius(GameObject g)
    {
        MonstersInRadius.Remove(g);
        MonsterLeftRadius?.Invoke(g);
    }

    private void OnDestroy()
    {
        foreach (var monstersInRadius in MonstersInRadius)
        {
            if (monstersInRadius.gameObject != null)
            {
                monstersInRadius.gameObject.GetComponent<Hitpoints>().OnMonsterDeath -= OnMonsterDeath;
                monstersInRadius.gameObject.GetComponent<MonsterPortalController>().OnMonsterReachedPortal -= OnMonsterDeath;
            }
        }
    }

    public void CleanUp()
    {
        MonstersInRadius.Clear();
    }
}
