﻿using UnityEngine;

public abstract class AreaDebuffer : MonoBehaviour
{
    protected AttackRadiusTrigger _areaAttackController;

    void Start()
    {
        _areaAttackController = GetComponent<AttackRadiusTrigger>();
        _areaAttackController.MonsterEnteredRadius += MonsterEnteredRadius;
        _areaAttackController.MonsterLeftRadius += MonsterLeftRadius;
    }

    protected abstract void MonsterLeftRadius(GameObject obj);

    protected abstract void MonsterEnteredRadius(GameObject obj);

    protected void OnDisable()
    {
        _areaAttackController.MonsterEnteredRadius -= MonsterEnteredRadius;
        _areaAttackController.MonsterLeftRadius -= MonsterLeftRadius;
    }
}
