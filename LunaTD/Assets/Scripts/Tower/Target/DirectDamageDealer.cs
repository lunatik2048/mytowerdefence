﻿using UnityEngine;

[RequireComponent(typeof(DamageComponent))]
public class DirectDamageDealer : MonoBehaviour, IAttacker
{
    private DamageComponent _dmg;

    void Awake()
    {
        _dmg = GetComponent<DamageComponent>();
    }

    public void Attack(GameObject g, StatisticsCounter counter)
    {
        g.GetComponent<Hitpoints>().DealDamage(_dmg.Damage, counter);
    }
}
