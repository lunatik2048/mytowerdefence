﻿using UnityEngine;

public class SimpleTargetFinder : TargetFinder
{
    public override GameObject[] GetTargets()
    {
        if (_validTargets.MonstersInRadius.Count != 0)
        {
            return new GameObject[]{ _validTargets.MonstersInRadius[0]};
        }
        else
        {
            return null;
        }
    }
}
