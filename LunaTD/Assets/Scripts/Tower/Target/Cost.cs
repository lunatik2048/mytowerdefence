﻿using UnityEngine;

public class Cost : MonoBehaviour
{
    [SerializeField] private int _cost;
    [SerializeField] private string _name;

    public int MoneyCost
    {
        get { return _cost;}
    }

    public string Name
    {
        get { return _name; }
    }
}
