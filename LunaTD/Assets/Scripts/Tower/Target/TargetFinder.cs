﻿using UnityEngine;

[RequireComponent(typeof(AttackRadiusTrigger))]
public abstract class TargetFinder : MonoBehaviour
{
    protected AttackRadiusTrigger _validTargets;

    protected void Awake()
    {
        _validTargets = GetComponent<AttackRadiusTrigger>();
    }

    public abstract GameObject[] GetTargets();
}
