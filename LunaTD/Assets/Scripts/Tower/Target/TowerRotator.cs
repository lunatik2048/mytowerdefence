﻿using System.Collections;
using UnityEngine;

public class TowerRotator : MonoBehaviour
{
    [SerializeField] private float _turnSpeed;
    [SerializeField] private Transform _objToRotate;

    private CooldownAttackController _attackController;
    private bool _turn;

    void Start()
    {
        _attackController = GetComponent<CooldownAttackController>();
        _attackController.TargetChanged += TargetChanged;
    }

    private void TargetChanged(GameObject[] obj)
    {
        if (obj != null)
        {
            if (_turn == false)
            {
                _turn = true;
                StartCoroutine(StartTurning());
            }
        }
        else
        {
            _turn = false;
        }
    }

    IEnumerator StartTurning()
    {
        while (_turn)
        {
            try
            {
                var targetRotation = Quaternion.LookRotation(_attackController.Targets[0].transform.position - _objToRotate.position);
                _objToRotate.rotation = Quaternion.Slerp(_objToRotate.rotation, targetRotation, _turnSpeed * Time.deltaTime);
                var rot = _objToRotate.rotation;
                rot.z = 0;
                rot.x = 0;
                _objToRotate.rotation = rot;
            }
            catch (System.Exception)
            {
                throw ;
            }
            yield return null;
        }
    }
}
