﻿using System.Collections.Generic;
using UnityEngine;

public class ClosestToEndTargetFinder : TargetFinder
{
    [SerializeField] private int _targets;

    protected void Awake()
    {
        base.Awake();
    }

    public override GameObject[] GetTargets()
    {
        if (_validTargets.MonstersInRadius.Count != 0)
        {
            var movement = ConvertToNavMonsterMovement(_validTargets.MonstersInRadius);
            var gameObjects = FindKBiggestNumbersM(movement, _targets);
            return gameObjects;
        }
        else
        {
            return null;
        }
    }

    private GameObject[] FindKBiggestNumbersM(NavMonsterMovement[] testArray, int k)
    {
        var arrayLength = testArray.Length >= k ? k : testArray.Length;

        var result = new NavMonsterMovement[arrayLength];

        int indexMin = 0;
        result[indexMin] = testArray[0];
        var min = result[indexMin].DistanceTraveled;

        for (int i = 1; i < testArray.Length; i++)
        {
            if (i < k)
            {
                var f = result[indexMin].DistanceTraveled;
                result[i] = testArray[i];
                if (f < min)
                {
                    min = f;
                    indexMin = i;
                }
            }
            else if (testArray[i].DistanceTraveled > min)
            {
                min = testArray[i].DistanceTraveled;
                result[indexMin] = testArray[i];
                for (int r = 0; r < k; r++)
                {
                    if (result[r].DistanceTraveled < min)
                    {
                        min = result[r].DistanceTraveled;
                        indexMin = r;
                    }
                }
            }
        }

        var res = new GameObject[arrayLength];
        for (int i = 0; i < res.Length; i++)
        {
            res[i] = result[i].gameObject;
        }

        return res;
    }

    private NavMonsterMovement[] ConvertToNavMonsterMovement(List<GameObject> objects)
    {
        var res = new NavMonsterMovement[objects.Count];

        for (int i = 0; i < objects.Count; i++)
        {
            res[i] = objects[i].GetComponent<NavMonsterMovement>();
        }

        return res;
    }
}
