﻿using UnityEngine;

public class AOEObject : MonoBehaviour
{
    protected TargetFinder _targetFinder;
    protected SourceContainer _sourceContainer;
    protected IAttacker[] _attackers;

    protected void Awake()
    {
        _targetFinder = GetComponent<TargetFinder>();
        _sourceContainer = GetComponent<SourceContainer>();
        _attackers = GetComponents<IAttacker>();
    }

    protected void DealDamage()
    {
        var targets = _targetFinder.GetTargets();
        if (targets != null)
        {
            foreach (var t in targets)
            {
                for (int i = 0; i < _attackers.Length; i++)
                {
                    _attackers[i].Attack(t, _sourceContainer.Counter);
                }
            }
        }
    }
}
