﻿using UnityEngine;

public class AoeObjectSpawner : MonoBehaviour
{
    private ObjectPool _pool;

    protected void Awake()
    {
        _pool = GetComponent<ObjectPool>();
    }

    public void Spawn(Vector3 pos, Quaternion rot, StatisticsCounter stats)
    {
        var obj = _pool.GetPooledObject();
        obj.transform.position = pos;
        obj.transform.rotation = rot;
        obj.SetActive(true);
        obj.GetComponent<AOEInstantDamageObject>().Attack();
        obj.GetComponent<SourceContainer>().Counter = stats;
    }
}
