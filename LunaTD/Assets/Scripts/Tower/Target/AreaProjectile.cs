﻿using DG.Tweening;
using UnityEngine;

public class AreaProjectile : MonoBehaviour, ITargetWaiter
{
    private TargetFinder _targetFinder;
    private IAttacker[] _attackers;
    private DestructionController _destr;
    private SourceContainer _source;

    void Start()
    {
        _targetFinder = GetComponent<TargetFinder>();
        _attackers = GetComponents<IAttacker>();
        _destr = GetComponent<DestructionController>();
        _source = GetComponent<SourceContainer>();
    }

    public void SetTarget(GameObject target)
    {
        var pos = target.transform.position;
        transform.DOJump(pos, 0.5f, 1, 0.5f).onComplete += OnComplete;
    }

    private void OnComplete()
    {
        var targets = _targetFinder.GetTargets();
        if (targets != null)
        {
            foreach (var target in targets)
            {
                for (int i = 0; i < _attackers.Length; i++)
                {
                    _attackers[i].Attack(target, _source.Counter); 
                }
            }
        }
        _destr.CleanUpAndDestroy();
    }
}
