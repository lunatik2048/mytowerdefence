﻿using UnityEngine;

public class ProjectileSpawner : MonoBehaviour, IAttacker
{
    [SerializeField] private Transform _spawnPoint;

    private ObjectPool _pool;

    void Awake()
    {
        _pool = GetComponent<ObjectPool>();
    }

    public void Attack(GameObject g, StatisticsCounter counter)
    {
        var proj = _pool.GetPooledObject();
        proj.transform.position = _spawnPoint.position;
        proj.SetActive(true);
        proj.GetComponent<SourceContainer>().Counter = counter;
        proj.GetComponent<ITargetWaiter>().SetTarget(g);
    }
}
