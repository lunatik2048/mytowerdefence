﻿using UnityEngine;

public class SingleTargetAttackController : CooldownAttackController
{
    protected override void MonsterEnteredRadius(GameObject obj)
    {
        if (Targets == null)
        {
            TryGetTarget();
        }
    }
}
