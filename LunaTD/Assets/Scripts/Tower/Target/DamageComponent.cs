﻿using UnityEngine;

public class DamageComponent : MonoBehaviour
{
    [SerializeField] private float _damage;

    public float Damage => _damage;
}
