﻿using System.Collections;
using UnityEngine;

public class TowerRadius : MonoBehaviour
{
    [SerializeField] private float _radius;
    [SerializeField] private GameObject _radiusDisplayer;

    public float Radius
    {
        get { return _radius; }
    }

    void Awake()
    {
        _radiusDisplayer.transform.localScale = new Vector3(_radius, _radius, 1);
        HideRadius();
        StartCoroutine(ExpandRadius());
    }

    private IEnumerator ExpandRadius()
    {
        var delay = new WaitForSeconds(0.05f);
        var collider = GetComponent<SphereCollider>();
        collider.radius = 0f;
        var p = _radius / 5f;

        for (int i = 0; i < 5; i++)
        {
            collider.radius = collider.radius + p;
            yield return delay;
        }
        collider.radius = _radius;
    }

    public void ShowRadius()
    {
        _radiusDisplayer.SetActive(true);
    }

    public void HideRadius()
    {
        _radiusDisplayer.SetActive(false);
    }
}
