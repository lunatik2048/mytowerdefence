﻿
public class AOEDestructableObject : AOEObject
{
    private DestructionController _destr;

    protected void Awake()
    {
        base.Awake();
        _destr = GetComponent<DestructionController>();
    }

    protected void DealDamageAndDestroy()
    {
        DealDamage();
        _destr.CleanUpAndDestroy();
    }
}
