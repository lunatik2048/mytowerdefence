﻿using System;
using System.Collections;
using UnityEngine;

public abstract class CooldownAttackController : MonoBehaviour
{
    [SerializeField] private float _cooldown;

    private AttackRadiusTrigger _targets;
    private IAttacker[] _attackers;
    private TargetFinder _tf;
    private GameObject[] _currentTargets;

    public Action<GameObject[]> TargetChanged;
    private StatisticsCounter _counter;

    public GameObject[] Targets
    {
        get { return _currentTargets; }
        private set
        {
            _currentTargets = value;
            TargetChanged?.Invoke(_currentTargets);
        }
    }

    public float Cooldown
    {
        get { return _cooldown; }
    }

    void Awake()
    {
        _counter = GetComponent<StatisticsCounter>();
    }

    void Start()
    {
        _targets = GetComponent<AttackRadiusTrigger>();
        _attackers = GetComponents<IAttacker>();
        _tf = GetComponent<TargetFinder>();
        _targets.MonsterEnteredRadius += MonsterEnteredRadius;
        StartCoroutine(StartAttacking());
    }

    protected abstract void MonsterEnteredRadius(GameObject obj);

    protected void TryGetTarget()
    {
        var target = _tf.GetTargets();
        Targets = target;
    }

    IEnumerator StartAttacking()
    {
        while (true)
        {
            TryGetTarget();
            if (_currentTargets != null)
            {
                for (int i = 0; i < _currentTargets.Length; i++)
                {
                    foreach (var attacker in _attackers)
                    {
                        attacker.Attack(_currentTargets[i], _counter);
                    }

                    if (_currentTargets == null)
                    {
                        break;
                    }
                }

                yield return new WaitForSeconds(_cooldown);
            }
            else
            {
                while (_currentTargets == null)
                {
                    yield return null;
                }
            }
        }
    }
}
