﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainMechanic : MonoBehaviour,ITargetWaiter
{
    [SerializeField] private int _maxTargets;
    [SerializeField] private float _radius;
    [SerializeField] private Vector3 _offset;

    private IAttacker _attacker;
    private int _layerMask;
    private Dictionary<int,GameObject> _damagedGameObjects;
    private Transform _transform;
    private WaitForSeconds _wait;

    private int _currentTargets;
    private SourceContainer _source;

    void Awake()
    {
        _attacker = GetComponent<IAttacker>();
        _transform = transform;
        _layerMask = LayerMask.GetMask("Monster");
        _damagedGameObjects = new Dictionary<int, GameObject>(_maxTargets);
        _source = GetComponent<SourceContainer>();
        _wait = new WaitForSeconds(0.15f);
    }

    public void SetTarget(GameObject target)
    {
        StartCoroutine(StartChaining(target));
    }

    public IEnumerator StartChaining(GameObject target)
    {
        _damagedGameObjects.Clear();
        _currentTargets = 0;
        AttackTarget(target);

        while (_currentTargets < _maxTargets)
        {
            yield return new WaitForSeconds(0.15f);
            target = GetNextTarget();
            if (target == null)
                break;
            AttackTarget(target);
        }
        yield return _wait;
        gameObject.GetComponent<IDestructable>().Destroy();
    }

    private void AttackTarget(GameObject target)
    {
        _transform.position = target.transform.position + _offset;
        _attacker.Attack(target, _source.Counter);
        _damagedGameObjects.Add(target.GetInstanceID(), target);
        _currentTargets++;
    }

    private GameObject GetNextTarget()
    {
        GameObject target = null;
        float distance = float.MaxValue;
        var allOverlappingColliders = Physics.OverlapSphere(_transform.position, _radius, _layerMask);

        for (int i = 0; i < allOverlappingColliders.Length; i++)
        {
            var current = allOverlappingColliders[i];
            if (!_damagedGameObjects.ContainsKey(current.gameObject.GetInstanceID()))
            {
                var go = current.gameObject;
                var dist = Vector3.Distance(_transform.position, go.transform.position);
                if (dist < distance)
                {
                    target = current.gameObject;
                    distance = dist;
                }
            }
        }
        return target;
    }
}
