﻿using System;
using UnityEngine;

public class StatisticsCounter : MonoBehaviour
{
    public event Action<float> DamageChanged;

    public float Damage { get; private set; }

    public void DamageDealt(float dmg)
    {
        Damage += dmg;
        DamageChanged?.Invoke(Damage);
    }
}
