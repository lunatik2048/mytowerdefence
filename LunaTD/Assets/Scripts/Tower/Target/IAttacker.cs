﻿using UnityEngine;

public interface IAttacker
{
    void Attack(GameObject g, StatisticsCounter counter);
}
