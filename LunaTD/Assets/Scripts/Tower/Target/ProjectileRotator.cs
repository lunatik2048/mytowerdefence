﻿using UnityEngine;

public class ProjectileRotator : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed;

    void Update()
    {
        transform.Rotate(Vector3.left * (_rotationSpeed * Time.deltaTime));
    }
}
