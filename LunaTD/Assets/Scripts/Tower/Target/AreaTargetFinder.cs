﻿using UnityEngine;

public class AreaTargetFinder : TargetFinder
{
    public override GameObject[] GetTargets()
    {
        if (_validTargets.MonstersInRadius.Count == 0)
        {
            return null;
        }
        else
        {
            return _validTargets.MonstersInRadius.ToArray();
        }
    }
}
