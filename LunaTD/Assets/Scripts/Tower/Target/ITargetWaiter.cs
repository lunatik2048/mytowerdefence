﻿using UnityEngine;

public interface ITargetWaiter
{
    void SetTarget(GameObject target);
}
