﻿using UnityEngine;

public class AreaAttackController : CooldownAttackController
{
    protected override void MonsterEnteredRadius(GameObject obj)
    {
        TryGetTarget();
    }
}
