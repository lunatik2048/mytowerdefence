﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private GameObject _prefab;
    [SerializeField] private int _numOfObjects;
    public Transform poolParent;

    private Queue<GameObject> _pool;

    protected void Start()
    {
        _pool = new Queue<GameObject>(_numOfObjects);
        for (int i = 0; i < _numOfObjects; i++)
        {
            CreateObj();
        }
    }

    public GameObject GetPooledObject()
    {
        if (_pool.Count == 0)
        {
            CreateObj();
        }
        return _pool.Dequeue();
    }

    public void AddToPool(GameObject go)
    {
        _pool.Enqueue(go);
    }

    private void CreateObj()
    {
        GameObject newObj;
        if (poolParent != null)
            newObj = Instantiate(_prefab, poolParent);
        else
            newObj = Instantiate(_prefab);
        newObj.SetActive(false);
        newObj.GetComponent<PooledObject>().Initialize(this);
        AddToPool(newObj);
    }

    void OnDestroy()
    {
        if (_pool != null)
        {
            foreach (var go in _pool)
            {
                if (go != null)
                    Destroy(go);
            }
        }
    }
}
