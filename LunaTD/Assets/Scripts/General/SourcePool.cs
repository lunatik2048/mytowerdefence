﻿using UnityEngine;

public class SourcePool : ObjectPool
{
    void Start()
    {
        base.Start();
        poolParent = GetComponent<SourceContainer>().Counter.transform;
    }
}
