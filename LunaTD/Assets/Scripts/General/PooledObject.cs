﻿using UnityEngine;

public class PooledObject : MonoBehaviour, IDestructable
{
    private ObjectPool _objectPool;

    public void Initialize(ObjectPool objectPool)
    {
        _objectPool = objectPool;
    }

    private void ReturnToPool()
    {
        if (_objectPool!=null)
        {
            _objectPool.AddToPool(gameObject); 
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Destroy()
    {
        gameObject.SetActive(false);
        ReturnToPool();
    }
}
