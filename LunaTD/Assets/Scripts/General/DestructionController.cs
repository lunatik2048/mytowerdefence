﻿using UnityEngine;

public class DestructionController : MonoBehaviour
{
    protected bool _isDestrCalled;
    private ICleanUp[] _cleanUps;
    private IDestructable _destr;

    void Awake()
    {
        _cleanUps = GetComponents<ICleanUp>();
        _destr = GetComponent<IDestructable>();
    }

    public void CleanUpAndDestroy()
    {
        if (!_isDestrCalled)
        {
            _isDestrCalled = true;
            foreach (var cleanUp in _cleanUps)
            {
                cleanUp.CleanUp();
            }
            _destr.Destroy(); 
        }
    }

    void OnEnable()
    {
        _isDestrCalled = false;
    }
}
