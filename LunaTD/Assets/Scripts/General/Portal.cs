﻿using UnityEngine;

public class Portal : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponent<MonsterPortalController>().Reached();
    }
}
