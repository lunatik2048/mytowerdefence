﻿
public class Constants
{
    public static float CellSize = 1f;
    public static float FlourHeight = 0f;
    public static float DestinationReachThreshold = 0.1f;

    public static string WalkSpeedAnim = "WalkSpeed";
    public static string DeadAnim = "Dead";

    public static string LevelsLocation = "Assets/CustomLevels/";
    public static string Json = ".json";

    public static int MonsterLayer = 9;
    public static string SelectableLayerName = "Selectable";
    public static string PathLayerName = "PathCell";
}
