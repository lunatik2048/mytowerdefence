﻿using UnityEngine;

public class ParentContainer : MonoBehaviour
{
    [SerializeField] private Transform _towerParent;
    [SerializeField] private Transform _particlesParent;
    [SerializeField] private Transform _monstersParent;

    public Transform TowerParent { get { return _towerParent;}}

    public Transform ParticlesParent { get { return _particlesParent; } }

    public Transform MonstersParent { get { return _monstersParent; } }

    public static ParentContainer Instance;

    void Awake()
    {
        Instance = this;
    }
}
