﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader
{
    public static void LoadMainScene()
    {
        SceneManager.LoadScene("MainScene");
    }

    public static void LoadMainMenuScene()
    {
        TimeManager.ResumeGame();
        SceneManager.LoadScene("MainMenu");
    }

    public static void QuitGame()
    {
        Application.Quit();
    }
}
