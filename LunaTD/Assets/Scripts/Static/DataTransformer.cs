﻿using UnityEngine;

public static class DataTransformer
{
    public static string LvlName { get; private set; }

    public static LevelInfo Lvl { get; private set; }

    public static void SetData(string lvlName, LevelInfo lvl)
    {
        LvlName = lvlName;
        Lvl = lvl;
    }
}
