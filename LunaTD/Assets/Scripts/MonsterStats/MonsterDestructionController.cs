﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class MonsterDestructionController : MonoBehaviour, IDestructable
{
    [SerializeField] private GameObject _hpBar;
    [SerializeField] private Hitpoints _hp;

    private CapsuleCollider _colider;
    private NavMonsterMovement _movement;
    private NavMeshAgent _agent;

    void Awake()
    {
        _colider = GetComponent<CapsuleCollider>();
        _movement = GetComponent<NavMonsterMovement>();
        _agent = GetComponent<NavMeshAgent>();
        _hp = GetComponent<Hitpoints>();
    }

    public void OnDeath()
    {
        WaitTillDeathAnim();
    }

    private void WaitTillDeathAnim()
    {
        _colider.enabled = false;
        _agent.enabled = false;
        _hpBar.SetActive(false);
        _movement.Stop();

        StartCoroutine(StartDeathAnimation());
    }

    protected virtual IEnumerator StartDeathAnimation()
    {
        yield return new WaitForSeconds(0.5f);
        OnDestruction();
    }

    public void OnDestruction()
    {
        Destroy(gameObject);
    }

    public void Destroy()
    {
        if (_hp.currentHP <= 0)
        {
            OnDeath();
        }
        else
        {
            OnDestruction();
        }
    }
}
