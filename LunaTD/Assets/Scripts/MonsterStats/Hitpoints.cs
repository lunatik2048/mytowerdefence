﻿using System;
using UnityEngine;

public class Hitpoints : DestructionController
{
    public float maxHP;
    public float currentHP;

    public Action<float> OnHitpointsChanged;
    public Action<GameObject> OnMonsterDeath;

    public bool IsDeathCalled
    {
        get { return _isDestrCalled; }
    }

    public void DealDamage(float damage, StatisticsCounter counter)
    {
        if (!_isDestrCalled)
        {
            currentHP -= damage;
            counter.DamageDealt(damage);
        }
        if (currentHP <= 0)
        {
            if (!_isDestrCalled)
            {
                currentHP = 0;
                OnHitpointsChanged?.Invoke(currentHP);
                OnMonsterDeath?.Invoke(gameObject);
                CleanUpAndDestroy();
            }
        }
        else
        {
            OnHitpointsChanged?.Invoke(currentHP);
        }
    }

    public float GetPercentage()
    {
        return currentHP / maxHP;
    }
}

