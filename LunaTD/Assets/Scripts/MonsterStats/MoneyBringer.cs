﻿using UnityEngine;

public class MoneyBringer : MonoBehaviour
{
    private MoneyManager _moneyManager;

    public int Money { get; private set; }

    void Start()
    {
        gameObject.GetComponent<Hitpoints>().OnMonsterDeath += OnMonsterDeath;
    }

    public void Setup(int money, MoneyManager moneyManager)
    {
        Money = money;
        _moneyManager = moneyManager;
    }

    private void OnMonsterDeath(GameObject obj)
    {
        _moneyManager.AddMoney(Money);
    }
}
