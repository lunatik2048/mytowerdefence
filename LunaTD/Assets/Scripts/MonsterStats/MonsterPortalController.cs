﻿using System;
using UnityEngine;

public class MonsterPortalController : MonoBehaviour
{
    public int MonsSpawned { get; set; }
    public Action<GameObject> OnMonsterReachedPortal;

    public void Reached()
    {
        OnMonsterReachedPortal?.Invoke(gameObject);
        gameObject.GetComponent<Hitpoints>().CleanUpAndDestroy();
    }
}
