﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class NavMonsterMovement : MonoBehaviour
{
    private float _baseSpeed;
    private float _minSpeed;
    private NavMeshAgent _agent;
    private Transform _transform;

    private float _decreaseMulty;
    private float _increaseMulty;
    private float _stopMult;

    private ISlowDebuff _currentDebuff;
    private DebuffManager _debuffManager;

    private Vector3 _prevPosition;
    private WaitForSeconds _wait;

    public float DistanceTraveled { get; set; }

    public event Action<float> SpeedChanged;

    public float CurrentUnitSpeed { get; private set; }

    private float SpeedDecreaseMulty
    {
        get { return _decreaseMulty; }
        set
        {
            _decreaseMulty = value;
            UpdateSpeed();
        }
    }

    private float SpeedIncreaseMulty
    {
        get { return _increaseMulty; }
        set
        {
            _increaseMulty = value;
            UpdateSpeed();
        }
    }

    void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _debuffManager = GetComponent<DebuffManager>();
        _transform = transform;
        _prevPosition = _transform.position;
        _wait = new WaitForSeconds(0.1f);
    }

    public void SetDestination(Vector3 endPoint, float speed)
    {
        _minSpeed = 0.5f;
        _baseSpeed = speed;
        _decreaseMulty = 1f;
        _stopMult = 1f;
        SpeedIncreaseMulty = 1f;

        _agent.SetDestination(endPoint);
        StartCoroutine(CalculateDistanceTraveled());
    }

    public IEnumerator CalculateDistanceTraveled()
    {
        while (true)
        {
            yield return _wait;
            DistanceTraveled += Vector3.Distance(_prevPosition, _transform.position);
            _prevPosition = _transform.position;
        }
    }

    public void ChangeIncMsMulti(float multi)
    {
        SpeedIncreaseMulty *= multi;
    }

    public void Stop()
    {
        _stopMult = 0;
        UpdateSpeed();
    }

    public void ContinueMoving()
    {
        _stopMult = 1;
        UpdateSpeed();
    }

    public void ApplySlowDebuff<T>(T debuff) where T : DebuffBase, ISlowDebuff
    {
        if (_currentDebuff == null)
        {
            ApplySlow(debuff);
        }
        else
        {
            if (debuff.GetSlowForce() > _currentDebuff.GetSlowForce())
            {
                RemoveSlow();
                ApplySlow(debuff);
            }
        }
    }

    public void RemoveSlowDebuff<T>(T debuff) where T : DebuffBase, ISlowDebuff
    {
        if (_currentDebuff == debuff)
        {
            RemoveSlow();
            var slows = _debuffManager.FindAllDebuffsOfType(DebuffType.Slow);
            if (slows.Count != 0)
            {
                T strongestSlow = null;
                float maxForce = 0f;
                for (int i = 0; i < slows.Count; i++)
                {
                    var force = (slows[i] as ISlowDebuff).GetSlowForce();
                    if (force > maxForce)
                    {
                        strongestSlow = (T)slows[i];
                        maxForce = force;
                    }
                }
                ApplySlow(strongestSlow);
            }
        }
    }

    private void ApplySlow<T>(T debuff) where T : DebuffBase, ISlowDebuff
    {
        _currentDebuff = debuff;
        SpeedDecreaseMulty = 1 - debuff.GetSlowForce();
    }

    private void RemoveSlow()
    {
        _currentDebuff = null;
        SpeedDecreaseMulty = 1f;
    }

    private void UpdateSpeed()
    {
        CurrentUnitSpeed = _baseSpeed * Mathf.Clamp(SpeedDecreaseMulty, _minSpeed, 1f) * SpeedIncreaseMulty * _stopMult;
        _agent.speed = CurrentUnitSpeed;
        SpeedChanged?.Invoke(CurrentUnitSpeed);
    }
}
